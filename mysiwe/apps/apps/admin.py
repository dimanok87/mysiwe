from django.contrib import admin
from .models import App, Settings


class AppAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}


class SettingsAdmin(admin.ModelAdmin):
    list_display = ('user', 'app')
    list_filter = ('app',)


admin.site.register(App, AppAdmin)
admin.site.register(Settings, SettingsAdmin)
