# -*- coding: utf-8 -*-
from django import forms
from .models import Settings


class SettingsForm(forms.ModelForm):
    class Meta:
        model = Settings
        fields = ('settings',)