from django.db import models
from django.conf import settings
from jsonfield import JSONField


class App(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name


class Settings(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='apps_settings')
    app = models.ForeignKey(App)
    settings = JSONField()

    class Meta:
        unique_together = (('user', 'app'))
        verbose_name_plural = 'Settings'

    def __unicode__(self):
        return '%s - %s' % (self.user, self.app)
