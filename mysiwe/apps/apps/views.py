from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import FormView
from mysiwe.utils import LoginRequired
from .models import App, Settings
from .forms import SettingsForm


class SettingsView(LoginRequired, FormView):
    form_class = SettingsForm

    def dispatch(self, request, *args, **kwargs):
        self.settings = self.get_settings(kwargs.get('slug'))
        return super(SettingsView, self).dispatch(request, *args, **kwargs)

    def get_settings(self, slug):
        app = get_object_or_404(App, slug=slug)

        return Settings.objects.get_or_create(app=app, user=self.request.user)[0]

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'status': 'ok',
            'settings': self.settings.settings,
        })

    def form_valid(self, form):
        self.settings.settings = form.cleaned_data['settings']
        self.settings.save()

        return JsonResponse({
            'status': 'ok',
            'message': 'Settings saved',
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'error': form.errors.get('settings', '')
        })
