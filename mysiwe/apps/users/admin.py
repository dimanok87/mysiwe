# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from .forms import UserChangeForm, UserCreationForm
from .models import User


class UserAdmin(DjangoUserAdmin):
    list_display = ('name', 'email', 'date_joined', 'is_active')
    search_fields = ('email',)
    ordering = ('-date_joined',)
    form = UserChangeForm
    add_form = UserCreationForm

    fieldsets = (
        (None, {'fields': ('name', 'email', 'password')}),
        ('Settings', {'fields': ('settings',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'email', 'password1', 'password2')
        }),
    )

admin.site.register(User, UserAdmin)
