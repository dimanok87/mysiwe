from django.conf.urls import patterns, url
from .views import LoginView, LogoutView, SettingsView, RegistrationView


urlpatterns = patterns('',
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^registration/$', RegistrationView.as_view(), name='registration'),
    url(r'^settings/$', SettingsView.as_view(), name='settings')
)