from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic import FormView, View
from .forms import UserSettingsForm, RegistrationForm
from mysiwe.utils import LoginRequired, get_form_errors


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = 'login.html'

    def form_invalid(self, form):
        if not self.request.is_ajax():
            return super(LoginView, self).form_invalid(form)
        else:
            return JsonResponse({
                'status': 'error',
                'error': 'Invalid login or password'
            })

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        if not self.request.is_ajax():
            return HttpResponseRedirect('/')
        else:
            return JsonResponse({'status': 'ok'})


class LogoutView(View):
    http_method_names = ['post', 'get']

    def get(self, request, *args, **kwargs):
        auth_logout(request)

        if request.is_ajax():
            return JsonResponse({'status': 'ok'})
        else:
            return HttpResponseRedirect('/')


class RegistrationView(FormView):
    form_class = RegistrationForm
    http_method_names = ['post']

    def form_valid(self, form):
        form.save()

        return JsonResponse({
            'status': 'ok',
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'errors': get_form_errors(form),
        })


class SettingsView(LoginRequired, FormView):
    form_class = UserSettingsForm

    def get(self, request, *args, **kwargs):
        return JsonResponse({
            'status': 'ok',
            'settings': request.user.settings,
        })

    def form_valid(self, form):
        form.save()
        return JsonResponse({
            'status': 'ok',
            'message': 'Settings saved',
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'error': form.errors.get('settings', '')
        })
