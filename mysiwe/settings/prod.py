from .base import *

STATICFILES_DIRS = ()

STATIC_ROOT = os.path.join(BASE_DIR, 'static')