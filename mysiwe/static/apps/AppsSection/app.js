var appsSection = $("<div>").addClass('apps-section-app');
var appsContent = $("<div>").addClass('apps-section-content').appendTo(appsSection);

var publicMethods = {
    append: function(options, application) {
        appsContent.append(options["content"]);
    },
    getSize: function(cb) {
        cb({
            width: appsContent.width(),
            height: appsContent.height()
        });
    }

};

this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};


this._constructor = function() {
    this.call('Desktop', 'append', {
        content: appsSection
    });
    this.onInit();
};