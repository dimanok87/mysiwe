var topBarButton =      $('<div>').addClass('audio-app fa fa-volume-up');
var volumeChanger =     $('<div>').addClass('volume-changer');
var changerLine =       $('<div>').addClass('volume-changer-line').appendTo(volumeChanger);
var volumeValueLine =   $('<div>').addClass('volume-value-line').appendTo(changerLine);
var changerMoving =     $('<div>').addClass('volume-changer-moving').appendTo(volumeValueLine);
var volumeChangeMask =  $('<div>').addClass('volume-changer-mask');
var soundValue = 45;
var startMovePosition = 0;
var heightOnePercent = false;

volumeValueLine.height(soundValue + '%');

var oldShowedVolume = soundValue;

var onchangeVolume = function(value) {
    if (oldShowedVolume <= 0 && value != 0) {
        topBarButton.removeClass('fa-volume-off');
        if (value < 80) {
            topBarButton.addClass('fa-volume-down');
        } else {
            topBarButton.addClass('fa-volume-up');
        }
    } else if (oldShowedVolume < 80 && (value >= 80 || value == 0)) {
        topBarButton.removeClass('fa-volume-down');
        topBarButton.addClass(value >= 80 ? 'fa-volume-up' : 'fa-volume-off');
    } else if (value <= 80) {
        topBarButton.removeClass('fa-volume-up');
        topBarButton.addClass(value != 0 ? 'fa-volume-down' : 'fa-volume-off');
    }
    volumeValueLine.height(value + '%');
    AudioContextObject.setVolume(oldShowedVolume = value);
};
var startMove = function(e) {
    e.preventDefault();
    startMovePosition = e.clientY;
    changerMoving.off('mousedown', startMove);
    heightOnePercent = changerLine.height() / 100;
    win.on({
        'mousemove': moveWind,
        'mouseup': moveEnd
    });
    volumeChangeMask.appendTo(body);
};
var moveWind = function(e) {
    e.preventDefault();
    var newValue = Math.min(100, Math.max(0, soundValue + (startMovePosition - e.clientY) / heightOnePercent));
    onchangeVolume(newValue);
};
var moveEnd = function(e) {
    e.preventDefault();
    soundValue = Math.min(100, Math.max(0, soundValue + (startMovePosition - e.clientY) / heightOnePercent));
    win.off({
        'mousemove': moveWind,
        'mouseup': moveEnd
    });
    volumeChangeMask.detach();
    changerMoving.on('mousedown', startMove);
};
changerMoving.on('mousedown', startMove);


//==================================================================================================================//

var AudioContextObject = new (function() {
//---- Работа с audioContext ----
    var context = false;
    try {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        context = new AudioContext();
    }
    catch(e) {}
//--- Подключаем колонки (получатель звука) ---
    var destination = context.destination;
//--- Создаём глобальный усилитель звука ---
    var gainNode = context.createGain();
//--- Устанавливаем громкость по умолчанию ---
    gainNode.gain.value = soundValue / 100;
//--- Подключаем получатель звука ---
    this.setVolume = function(vol) {
        gainNode.gain.value = vol / 100;
    };
    this.createSource = function(options) {
        options = $.extend({
            analyser: false,
            source: 'audio'
        }, options);
        var source, analyser, mediaElementSource, equalizers;
        var scriptNode = context.createScriptProcessor(4096);
        switch (options.source) {
            case 'audio':
                source = new Audio();
                mediaElementSource = context.createMediaElementSource(source);
                break;
        }
        //--- Подключаем получатель звука для источника ---

        if (options.analyser) {
            analyser = context.createAnalyser();
            var fFrequencyData = new Float32Array(analyser.frequencyBinCount),
                bFrequencyData = new Uint8Array(analyser.frequencyBinCount),
                bTimeData = new Uint8Array(analyser.frequencyBinCount);
            analyser.smoothingTimeConstant = options.analyser['timeOut'] || 0.3;
            analyser.fftSize = options.analyser['fftSize'] || 2048;
            var audioProcess = function() {
                analyser.getFloatFrequencyData(fFrequencyData);
                analyser.getByteFrequencyData(bFrequencyData);
                analyser.getByteTimeDomainData(bTimeData);
                if (typeof options.analyser.update === "function") {
                    options.analyser.update(bFrequencyData, fFrequencyData, bTimeData);
                }
            };
            $(source).bind('play', function() {
                scriptNode.onaudioprocess = audioProcess;
            });
            $(source).bind('pause abort stop end', function() {
                scriptNode.onaudioprocess = false;
            });


        }

        if (options.equalizer) {
            //--- Создание одного частотного фильтра ---
            var z = 0;
            var createFilter = function(frequency) {
                var filter = context.createBiquadFilter();
                filter.type = 'peaking';
                filter.frequency.value = frequency;
                filter.Q.value = 1;
                filter.gain.value = 10;
                z++;
                return filter;
            };

            //--- Создание всех частотных фильтров ---
            var createFilters = function () {
                var frequencies = [60, 170, 310, 600, 1000, 3000, 6000, 12000, 14000, 16000],
                    filters = frequencies.map(createFilter);
                filters.reduce(function (prev, curr) {
                    prev.connect(curr);
                    return curr;
                });
                return filters;
            };

            //--- Соединение фильтров ----
            equalizers = createFilters();
        }

        //--- Подключение фильтров ----

        //--- Подключаем плеер к эквалайзеру ---
        mediaElementSource.connect(equalizers[0]);

        //--- Эквалайзер к усилителю звука ---
        equalizers[equalizers.length - 1].connect(gainNode);

        //--- Подключаем усилитель звука к колонкам ---
        gainNode.connect(destination);

        //--- Подключаем эквалайзер к визуализации ---
        equalizers[equalizers.length - 1].connect(analyser);
        mediaElementSource.connect(scriptNode);
        scriptNode.connect(analyser);

        return {
            audioNode : source,
            equalizer: equalizers,
            analyser: analyser
        };
    };
})();

var publicMethods = {
    getAudioSource: function(options) {
        options.callback(AudioContextObject.createSource(options));
    }
};

this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        return publicMethods[appMethod](callOptions, subjectApp);
    }
};


//==================================================================================================================//

this._constructor = function() {
    this.call('TopBar', 'append', {
        content: topBarButton
    });
    this.call('TopBar', 'addContentToButton', {
        content: volumeChanger
    });
    this.onInit();
};
