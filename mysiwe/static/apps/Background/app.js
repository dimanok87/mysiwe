var app = this;
var defaults = {
    size: 'cover'
};
var body = $('body:first');
var onLoadBg = function() {
    body.css({
        'background-image': 'url(' + this.src + ')'
    });
};
var setSizeBG = function(mode) {
    body.addClass('bg-mode-' + mode);
};

var publicMethods = {
    'setBackground': function(options, application) {
        setBackground(options['src']);
    }
};

this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};

var setBackground = function(src) {
    var img = new Image();
    $(img).bind('load', onLoadBg);
    img.src = src;
};

this._constructor = function(bgImage) {
    setSizeBG(defaults['size']);
    bgImage ? setBackground(bgImage.src) : false;
    this.onInit();
};
