var clock = $("<div>").addClass('clock-app');
var extensions = ['Calendar'];

this.loadExtensions(extensions);

var hoursBlock = $("<span>").addClass("hours-block top-button-block").appendTo(clock),
    delimiterBlock = $("<span>").addClass("delimiter-block top-button-block").appendTo(clock).text(':'),
    minutesBlock = $("<span>").addClass("minutes-block top-button-block").appendTo(clock);

var updateTime = function() {
    var dateTime = new Date();
    var hours = dateTime.getHours();
    var minutes = dateTime.getMinutes();
    hoursBlock.text((hours < 10 ? "0" : "") + hours);
    minutesBlock.text((minutes < 10 ? "0" : "") + minutes);
};

var showDelimiter = function() {
    delimiterBlock.removeClass("invisible");
    setTimeout(hideDelimiter, 700);
};

var hideDelimiter = function() {
    delimiterBlock.addClass("invisible");
    setTimeout(showDelimiter, 300);
};

this.addContentToButton = function(content) {
    this.call('TopBar', 'addContentToButton', {
        content: content
    });
};

this._constructor = function() {
    this.call('TopBar', 'append', {
        content: clock
    });
    updateTime();
    setInterval(updateTime, 25000);
    hideDelimiter();
    this.onInit();
};
