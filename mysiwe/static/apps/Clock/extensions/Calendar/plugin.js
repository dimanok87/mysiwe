var calendar = $('<div>').addClass('calendar-applet');
var daysNames = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
var fullDaysNames = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
var fullMonthsNames = [
    "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
    "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
var cells = [],
    calendarTable = $("<table>").appendTo(calendar),
    calendarTableBody = $('<tbody>').appendTo(calendarTable),
    dayNamesTR = $('<tr>').appendTo($('<thead>').appendTo(calendarTable));
var changerDate = $("<div>").addClass('changer-calendar-line').prependTo(calendar);
var toDay = $("<div>").addClass('calendar-today-text').prependTo(calendar);
var toDayText = $('<span>').appendTo(toDay).click(function() {
    setMonthYear(toDayDate.getMonth(), toDayDate.getFullYear());
});
for (var i = 0; i < 7; i++) {
    dayNamesTR.append($('<td>').text(daysNames[i]));
}
for (var r = 0; r < 6; r++) {
    var row = $('<tr>').appendTo(calendarTableBody);
    for (var c = 0; c < 7; c++) {
        var cell = $('<td>').appendTo(row).text((c + 1) + 7*r);
        cells.push(cell);
    }
}
var showedDate = new Date();
showedDate.setDate(1);
var toDayDate = new Date();
var applyTodayDate = function() {
    toDayDate = new Date();
    toDayText.text(
        fullDaysNames[toDayDate.getDay()] + ', ' +
        toDayDate.getDate() + ', ' +
        fullMonthsNames[toDayDate.getMonth()] + ', ' +
        toDayDate.getFullYear()
    );
};
var setMonthYear = function(m, y) {
    showedDate.setMonth(m);
    showedDate.setYear(y);
    var day = showedDate.getDay();
    day = (day || 7) - 1;
    visibleMonthName.text(fullMonthsNames[m]);
    visibleYearName.text(y);
    for (var i = 0; i < cells.length; i++) {
        var visibleDate = new Date(showedDate.getTime() + (i - day) * 24 * 3600000);
        cells[i].text(visibleDate.getDate()).data('date', visibleDate);
        if (visibleDate.getMonth() != showedDate.getMonth()) {
            cells[i].addClass('other-month');
        } else {
            cells[i].removeClass('other-month');
        }
    }
};
var visibleMonthName = $('<span>').addClass('calendar-month-name');
var nextMonthButton = $('<div>').addClass('changer-button next-button').click(function() {
    var vMonth = showedDate.getMonth() + 1;
    var nextMonth = vMonth % 12;
    setMonthYear(nextMonth, vMonth >= 12 ? showedDate.getFullYear() + 1 : showedDate.getFullYear());
});
var prevMonthButton = $('<div>').addClass('changer-button prev-button').click(function() {
    var vMonth = showedDate.getMonth() - 1;
    var nextMonth = (vMonth + 12) % 12;
    setMonthYear(nextMonth, nextMonth == 11 ? showedDate.getFullYear() - 1 : showedDate.getFullYear());
});
var visibleYearName = $('<span>').addClass('calendar-year-name');
var nextYearButton = $('<div>').addClass('changer-button next-button').click(function() {
    setMonthYear(showedDate.getMonth(), showedDate.getFullYear() + 1);
});
var prevYearButton = $('<div>').addClass('changer-button prev-button').click(function() {
    setMonthYear(showedDate.getMonth(), showedDate.getFullYear() - 1);
});
var monthChanger = $('<div>').addClass('month-changer').appendTo(changerDate);
var yearChanger = $('<div>').addClass('year-changer').appendTo(changerDate);
monthChanger.append(prevMonthButton, visibleMonthName, nextMonthButton);
yearChanger.append(prevYearButton, visibleYearName, nextYearButton);
setMonthYear(showedDate.getMonth(), showedDate.getFullYear());
setInterval(applyTodayDate, 300000);
applyTodayDate();
this._initialize = function(app, options) {
    toDay.prepend('<img src="' + options.path + '/calendar-icon.png" class="icon-calendar"/>');
    app.addContentToButton(calendar);
};
