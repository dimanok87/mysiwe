var jqElements = {
    "main-block": $("<div>").addClass('cloud-storage-app')
};

var discButton = function(params) {
    var button = $("<div>").addClass("storage-disc");
    var iconButton = $("<img>").addClass("icon-disc").attr({
        src: params["icon"]
    }).appendTo(button);
    var descButton = $("<div>").
        addClass("desc-disc").
        appendTo(button);
    var nameDisc = $("<span>").
        addClass("name-disc").
        appendTo(descButton).text(params["name"]);
    var shortDescButton = $("<span>").addClass("short-desc-disc");
    if (params["description"]) {
        shortDescButton.text(params["description"]).appendTo(descButton);
    }
    return button;
};

var activedContent = {};
var setContent = function(button, content) {
    if (content.is(activedContent["content"] || false)) {
        return;
    }
    if (activedContent["content"] || false)  {
        activedContent["content"].detach();
        activedContent["button"].removeClass("active");
    }
    activedContent["content"] = content;
    activedContent["button"] = button.addClass("active");
    filesList.append(content);
};

this.addDiscType = function(params) {
    var button = $("<div>").addClass("option-disc-type");
    button.append(
        $("<img>").addClass("option-disc-icon").attr("src", params.icon),
        $("<span>").addClass("option-disc-name").text(params.name)
    );
    button.appendTo(discsButtonsForAdding);
};

var mainBlock =         jqElements["main-block"];
var navigationPanel =   $("<div>").addClass("storage-navigation").appendTo(mainBlock);
var navNextPrev =       $("<div>").addClass("buttons-group").appendTo(navigationPanel);
var navNavigation =     $("<div>").addClass("buttons-group").appendTo(navigationPanel);
var prevButton =        $("<div>").addClass("button normal").appendTo(navNextPrev).text("<");
var nextButton =        $("<div>").addClass("button normal").appendTo(navNextPrev).text(">");
var discsList =         $("<div>").addClass("storage-discs-list").appendTo(mainBlock);
var filesList =         $("<div>").addClass("storage-files-list").appendTo(mainBlock);
var contentDiscsList =  $("<div>").addClass("storage-discs-content").appendTo(discsList);
var connectedDiscs =    $("<div>").addClass("storage-discs-connected").appendTo(contentDiscsList);

var addDiscButton =     discButton({
    icon: "images/apps/CloudStorage/add-cloud.png",
    name: "Добавить диск"
}).addClass("disc-adding").appendTo(contentDiscsList).click(function() {
    setContent(addDiscButton, discTypesFоrAdding);
});

var discTypesFоrAdding = $("<div>").addClass("adding-content");
var discsButtonsForAdding = $("<div>").addClass("discs-buttons-list").appendTo(discTypesFоrAdding);


var isMinimal = false;

var minimalDiscsListButton = $("<div>").addClass("minimal-discs-list").click(function() {
    isMinimal = !isMinimal;
    discsList[isMinimal ? "addClass" : "removeClass"]("minimal");
}).appendTo(discsList).append("<span>");

var application = this;
this._constructor = function() {
    application.call('Windows', 'Window', {content: jqElements['main-block']});
    this.onInit();
};
