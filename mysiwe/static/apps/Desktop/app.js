var containerDesktop = $("body");

var publicMethods = {
    append: function(options, application) {
        containerDesktop.append(options["content"]);
        options['success'] ? options['success']() : false;
    },
    prepend: function(options, application) {
        containerDesktop.prepend(options["content"]);
        options['success'] ? options['success']() : false;
    }
};

this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};

this._constructor = function() {
    this.onInit();
};


