var application = this;

var showForms = function() {
    var content = $("<div>").addClass("login-app");
    var form = $("<form>");
    var fields = {
        "login": $("<input>").attr({type: "text", tabindex: 1, placeholder: "E-mail or Login"}),
        "password": $("<input>").attr({type: "password", tabindex: 2, placeholder: "Your password"}),
        "conf_password": $("<input>").attr({type: "password", tabindex: 2, placeholder: "Confirm your password"})
    };

    var sendLoginForm = function(cb) {
        $.ajax({
            url: conf["urls"]["user"]["Login"]["url"],
            type: conf["urls"]["user"]["Login"]["method"],
            dataType: 'JSON',
            data: {
                username: fields['login'].val(),
                password: fields['password'].val()
            },
            success: function(data) {
                if (data['status'] == 'error') {
                    showError(data['error']);
                } else {
                    loadUserSettings(function(dt) {
                        if (dt['status'] != 'error') {
                            logInSuccess();
                            cb ? cb() : false;
                        } else {
                            showError(dt['message']);
                        }
                    });
                }
            }
        });
    };

    var sendRegForm = function(cb) {
        if (fields['password'].val() != fields['conf_password'].val()) {
            return showError('Entered passwords do not match');
        }
        $.ajax({
            url: conf["urls"]["user"]["Registration"]["url"],
            type: conf["urls"]["user"]["Registration"]["method"],
            dataType: 'JSON',
            data: {
                email: fields['login'].val(),
                password1: fields['password'].val(),
                password2: fields['conf_password'].val()
            },
            success: cb
        });
    };

    var showError = function(text) {
        rowsForm['btn'].before(errorRow.text(text));
    };
    var errorsTitles = {
        password1: 'Password',
        password2: 'Confirm password',
        email: 'E-mail'
    };
    form.on('submit', function(e) {
        e.preventDefault();
        errorRow.detach();
        modeForm == 'login' ? sendLoginForm() :
            sendRegForm(function(data) {
                if (data['status'] == 'error') {
                    var errorText = "";
                    for (var k in data['errors']) {
                        errorText+= errorsTitles[k] + ': ' + data['errors'][k] + ";\n"
                        showError(errorText);
                    }
                } else if (data['status'] == 'ok') {
                    sendLoginForm(function() {
                        application.call('Notices', 'Notice', {
                            text: 'Welcome to Mysiwe!',
                            title: 'Mysiwe'
                        });
                    });
                }
            });
    });

    var modeForm = 'login';
    var changeMode = function() {
        errorRow.detach();
        if (modeForm == 'login') {
            modeForm = 'registration';
            changeFormText.text('I`m already registered');
            rowsForm['pwd'].after(rowsForm['cpwd']);
        } else {
            modeForm = 'login';
            changeFormText.text('I`m new user');
            rowsForm['cpwd'].detach();
        }
    };
    var changeFormText = $('<span>').addClass('change-form-mode').text('I`m new user').on('click', changeMode);
    var errorRow = $("<div>").addClass("login-form-field-row error-row");
    var submitFormButton = $("<button>").attr({
        role: "button",
        tabindex: 3
    }).text("Send form").addClass('form-button');

    var rowsForm = {
        login:  $("<div>").addClass("login-form-field-row").append(fields["login"]),
        pwd:    $("<div>").addClass("login-form-field-row").append(fields["password"]),
        cpwd: $("<div>").addClass("login-form-field-row").append(fields["conf_password"]),
        btn: $("<div>").addClass("login-form-field-row submit-button-row").append(submitFormButton, changeFormText)
    };

    content.append(
        form.append(rowsForm['login'], rowsForm['pwd'], rowsForm['btn'])
    );

    var closeAnimateTime = 0.5, openAnimateTime = 0.5;

    content.css({transition: openAnimateTime + 's ease-in-out'});

    application.Event.addHandlers({
        onClose: function() {
            content.css({transition: closeAnimateTime + 's ease-in-out'}).addClass('close');
            setTimeout(function() {
                content.empty().remove();
            }, closeAnimateTime * 1000);
        }
    });
    application.call('Desktop', 'append', {
        'content': content,
        'success': function() {
            setTimeout(function(){
                content.addClass('open');
            }, 1);
            setTimeout(function() {
                content.css({transition: ''});
            }, openAnimateTime * 1000);
        }
    });
};

var logInSuccess = function() {
    application.call('OS');
    application.Close();
    return false;
};

var conf = this.config.getConfig();
var loadUserSettings = function(cb) {
    $.ajax({
        url: conf["urls"]["user"]["Settings"]["get"]["url"],
        type: conf["urls"]["user"]["Settings"]["get"]["method"],
        dataType: 'JSON',
        success: function(dt) {
            cb ? cb(dt) : false;
            if (dt['status'] == 'error') {
                switch(dt['code']) {
                    case 101:
                        showForms();
                        break;
                }
            } else {
                logInSuccess();
            }
        }
    });
};
this._constructor = function() {
    loadUserSettings(function(data) {
        if (data['status'] == 'error') {
            application.call('Background', 'setBackground', {
                src: '/static/images/background.jpg'
            });
        }
    });
    this.onInit();
};
