var player =                $("<div>").addClass('radio-app');
var listServices =          $("<div>").addClass('radio-app-list-services').appendTo(player);
var playListAndControl =    $("<div>").addClass('radio-main-block').appendTo(player);
var controlPanel =          $("<div>").addClass('control-panel').appendTo(playListAndControl);
var controlsButtons =       $("<div>").addClass('control-buttons').appendTo(controlPanel);
var playListOnly =          $('<div>').addClass('playlist-player').prependTo(playListAndControl);
var tracksList =            $('<table>').appendTo(playListOnly);
var equalizerPanel =        $("<div>").addClass('equalizer-panel');

var extensions = ['di.fm', 'radiotunes', 'jazz'/*, 'record'*/];

var emptyPlayListTR = $('<tr>').
    addClass('empty-list').
    append($('<td>').text('Список воспроизведения пуст')).appendTo(tracksList);
var appTitle = 'Музыкальный проигрыватель';

//---- Наделяем кнопки управления силой ----
var activeStation = false;
var playlist = [];
var playedItem = false;
var checkedItem = false;
var prevButtonStatus = false;
var nextButtonStatus = false;
var repeatOn = false;
var checkList = [];

var playCheckedItem = function() {
    if (checkedItem) {
        checkedItem.isPlayed() ?
            AudioJS.pause() :
            (checkedItem.isStopped() ? playItem(checkedItem) : AudioJS.play());
    }
};

var stopPlayedItem = function() {
    stopButton.addClass('disabled');
    AudioJS.src = '';
};

var playPrevItem = function() {
    if (playlist.length) {
        var newIndex = checkedItem.getIndex() - 1;
        if (newIndex < 0) {
            (repeatOn ? playlist.length - 1 : false) !== false ?
                playItem(playlist[playlist.length - 1]) :
                false;
        } else {
            playItem(playlist[newIndex]);
        }
    }
};

var playNextItem = function() {
    if (playlist.length) {
        var newIndex = checkedItem.getIndex() + 1;
        if (newIndex > playlist.length - 1) {
            (repeatOn ? 0 : false) !== false ?
                playItem(playlist[0]) :
                false;
        } else {
            playItem(playlist[newIndex]);
        }
    }
};

var checkPrevNextButtons = function() {
    if (repeatOn || checkedItem.getIndex() > 0) {
        if (!prevButtonStatus) {
            prevButton.removeClass('disabled');
            prevButtonStatus = true;
        }
    } else if (prevButtonStatus) {
        prevButton.addClass('disabled');
        prevButtonStatus = false;
    }
    if (repeatOn || checkedItem.getIndex() < playlist.length - 1) {
        if (!nextButtonStatus) {
            nextButton.removeClass('disabled');
            nextButtonStatus = true;
        }
    } else if (nextButtonStatus) {
        nextButton.addClass('disabled');
        nextButtonStatus = false;
    }
};

var changeRepeatMode = function() {
    repeatOn = !repeatOn;
    repeatButton[repeatOn ? 'addClass' : 'removeClass']('active');
    if (!playlist.length) {
        return;
    }
    checkPrevNextButtons(true);
};

var equalizerOpened = false;
var equalizerOnOff = function() {
    equalizerOpened = !equalizerOpened;
    equalizerButton[equalizerOpened ? 'addClass' : 'removeClass']('active');
    equalizerOpened ?
        equalizerPanel.appendTo(controlsButtons) :
        equalizerPanel.detach();
};

var playButton = $('<div>').
    addClass('player-control fa fa-play disabled').
    click(playCheckedItem).attr({
        role: 'button',
        tabindex: 10
    });

var stopButton = $('<div>').
    addClass('player-control fa fa-stop disabled').
    click(stopPlayedItem).attr({
        role: 'button',
        tabindex: 12
    });

var prevButton = $('<div>').
    addClass('player-control fa fa-fast-backward disabled').
    click(playPrevItem).attr({
        role: 'button',
        tabindex: 13
    });

var nextButton = $('<div>').
    addClass('player-control fa fa-fast-forward disabled').
    click(playNextItem).attr({
        role: 'button',
        tabindex: 14
    });

var deleteItems = function() {
    for (var i = 0; i < checkList.length; i++) {
        var newPlaylist = [];
        checkList[i].getJQ().detach();
        for (var k = 0; k < playlist.length; k++) {
            if (checkList[i] != playlist[k]) {
                newPlaylist.push(playlist[k]);
            }
        }
        playlist = newPlaylist;
    }
    checkList = [];
    deleteButton.addClass('disabled');
};

var deleteButton = $('<div>').
    addClass('player-control fa fa-trash disabled').
    click(deleteItems);
var repeatButton = $('<div>').
    addClass('player-control fa fa-repeat').
    click(changeRepeatMode);
var randomButton = $('<div>').
    addClass('player-control fa fa-random disabled');
var equalizerButton = $('<div>').
    addClass('player-control fa fa-sliders').on('click', equalizerOnOff);
var repeatRandomControl = $('<div>').
    addClass('controls-block left').
    append(repeatButton, randomButton).
    appendTo(controlsButtons);
var basedControls = $('<div>').
    addClass('controls-block left').
    append(prevButton, playButton, stopButton, nextButton).
    appendTo(controlsButtons);
var equalizerControl = $('<div>').
    addClass('controls-block left disabled').
    append(equalizerButton).
    appendTo(controlsButtons);
var editControl = $('<div>').
    addClass('controls-block right').
    append(deleteButton).
    prependTo(controlsButtons);

var impuls = 128;

var visualisations = [
    function(width, height, ctx, reini, data) {
        ctx.fillStyle = "rgba(32, 32, 32, 0.8)";
        ctx.fillRect(0, 0, width, height);
        var k = Math.round(width / (impuls * 0.75));
        var gradient = ctx.createLinearGradient(0, 0, 0, height);
        gradient.addColorStop(0.7, 'rgba(253, 252, 98, 0.8)');
        gradient.addColorStop(1, 'rgba(0, 200, 0, 0.8)');
        gradient.addColorStop(0, 'rgba(255, 100, 53, 0.8)');
        for (var i = 0; i < data[0].length; i++ ) {
            var val = data[0][i];
            ctx.fillStyle = gradient;
            ctx.fillRect(i*k,  height - (val / 2.55 * height / 110), k, height);
            ctx.beginPath();
            ctx.strokeStyle = "rgba(16, 16, 16, 1)";
            ctx.moveTo(i*k, 0);
            ctx.lineTo(i*k, height);
            ctx.stroke();
            ctx.closePath();
        }
        ctx.beginPath();
        for (var y = height; y > 0; y -= k) {
            ctx.moveTo(0, y);
            ctx.lineTo(width, y);
        }
        ctx.stroke();
        ctx.closePath();
    }
];
var activeVisualisation = 0;

var Analyser = function() {
    var analyser = $('<canvas>').addClass('music-analyse').appendTo(controlPanel);
    var DOMAnalyser = analyser.get(0);
    var ctx = DOMAnalyser.getContext("2d");
    var oldW = 0;
    var oldH = 0;
    ctx.lineWidth = 1;
    this.update = function(array, array2, array1) {
        var width = analyser.width();
        var height = analyser.height();
        var reini = false;
        if (oldW != width || oldH != height) {
            DOMAnalyser.width = oldW = analyser.width();
            DOMAnalyser.height = oldH = analyser.height();
        }
        visualisations[activeVisualisation](width, height, ctx, reini, [array, array2, array1]);
    };
};

var AnalyserObject = new Analyser();
var AudioJS;
var indexListItem = 0;

//---- Наделяем кнопки управления силой ----
this.setWindowName = function(title) {
    applicationWindow.setTitle(appTitle + ' (' + title + ')');
};

var defaultFiltersParams = {
    max: 16,
    min: -16,
    value: 0
};

var maskChanger = $('<div>').addClass('ranger-changer-mask');

var createOneRanger = function(opts) {
    opts = opts || {};
    var val = opts['val'];
    var rangerContainer =   $('<div>').addClass('ranger-container');
    var rangerChangeLine = $('<div>').addClass('ranger-changer-line').appendTo(rangerContainer);
    var rangerValueLine =   $('<div>').addClass('ranger-value-line').appendTo(rangerChangeLine);
    var changerMoving =     $('<div>').addClass('ranger-changer-moving').appendTo(rangerValueLine);
    var max = defaultFiltersParams['max'];
    var min = defaultFiltersParams['min'];
    var startMovePosition = 0;
    var heightOnePercent = false;
    var onchangeVolume = function(value) {
        opts.onchange ? opts.onchange(value) : false;
    };
    var startMove = function(e) {
        e.preventDefault();
        startMovePosition = e.clientY;
        changerMoving.off('mousedown', startMove);
        heightOnePercent = rangerChangeLine.height() / (max - min);
        win.on({
            'mousemove': moveWind,
            'mouseup': moveEnd
        });
        maskChanger.appendTo(body);
    };
    var newValue = 0;
    var moveWind = function(e) {
        e.preventDefault();
        newValue = Math.min(max, Math.max(min, val + (startMovePosition - e.clientY) / heightOnePercent));
        rangerValueLine.height((newValue - min) / ((max - min) / 100) + '%');
        onchangeVolume(newValue);
    };
    var moveEnd = function(e) {
        e.preventDefault();
        val = newValue;
        win.off({
            'mousemove': moveWind,
            'mouseup': moveEnd
        });
        maskChanger.detach();
        changerMoving.on('mousedown', startMove);
    };
    changerMoving.on('mousedown', startMove);
    rangerValueLine.height((val - min) / ((max - min) / 100) + '%');
    return rangerContainer;
};

var createSoundFilters = function(filters) {
    filters.forEach(function(filter) {
        filter.gain.value = defaultFiltersParams.value;
        var rangerContainer = createOneRanger({
            val: defaultFiltersParams['value'],
            onchange: function(val) {
                filter.gain.value = val;
            }
        });
        var oneFieldRanger = $('<div>').addClass('equalizer-field');
        var v = filter.frequency.value;
        oneFieldRanger.append(
            rangerContainer,
            $('<span>').addClass('label-equalizer-field').text(v < 1000 ? v + ' Hz' : v/1000 + ' kHz')
        ).appendTo(equalizerPanel);
    });
};

var iniAudioJS = function(ajs) {
    AudioJS = ajs['audioNode'];
    createSoundFilters(ajs['equalizer']);
    AudioJS.onplay = function() {
        playButton.removeClass('fa-play').addClass('fa-pause');
        playedItem.checkPlay();
    };
    AudioJS.onpause = function() {
        playButton.addClass('fa-play').removeClass('fa-pause');
        playedItem.Pause();
    };
    AudioJS.onabort = function() {
        playButton.addClass('fa-play').removeClass('fa-pause');
        playedItem.Stop();
        applicationWindow.setTitle(appTitle);
        playedItem = false;
    };
};
var activateChannelsList = function(button, list, container) {
    button.click(function() {
        if (activeStation) {
            activeStation['list'].detach();
            activeStation['button'].removeClass('opened');
            if (activeStation['button'].is(button)) {
                activeStation = false;
                return;
            }
        }
        activeStation = {
            button: button.addClass('opened'),
            list: list.appendTo(container)
        };
    })
};
this.setAudioSource = function(item, src) {
    playedItem ? playedItem.Stop() : false;
    playedItem = item;
    AudioJS.crossOrigin = "anonymous";
    AudioJS.src = '/static/media/audio/2.mp3'; // http://air2.radiorecord.ru:805/rr_320';//src;
    AudioJS.play();
};
var playItem = function(item) {
    stopButton.removeClass('disabled');
    if (playedItem != item) {
        checkedItem ? checkedItem.unCheck(true) : false;
        AudioJS.src = '';
        item.Check(true).Play();
    } else {
        playedItem.isPlayed() ? AudioJS.pause() : AudioJS.play();
    }
    checkedItem = item;
    checkPrevNextButtons();
};
var playListItem = function(params, cbs) {
    var index = indexListItem;
    indexListItem++;
    var itemDOM = $('<tr>').addClass('item-playlist');
    var playIcon = $('<div>').appendTo($('<td>').appendTo(itemDOM)).addClass('play-track fa');
    var nameTrack = $('<div>').appendTo($('<td>').appendTo(itemDOM)).addClass('name-track');
    var authorTrack = $('<div>').appendTo($('<td>').appendTo(itemDOM)).addClass('author-track');
    var lengthTrack = $('<div>').appendTo($('<td>').appendTo(itemDOM)).addClass('length-track');
    var iconStation = $('<div>').appendTo($('<td>').appendTo(itemDOM)).addClass('image-container');
    var title, artist;
    this.getIndex = function() {
        return index;
    };
    this.getOption = function(name) {
        return params[name] || false;
    };
    this.setNameTrack = function(name) {
        nameTrack.text(title = name);
    };
    this.setAuthorTrack = function(author) {
        authorTrack.text(artist = author);
    };
    this.setLengthTrack = function(length) {
        var min = Math.ceil(length / 60);
        var sec = length % 60;
        lengthTrack.text(length ? ((min < 10 ? '0' : '') + min) + ':' + ((sec < 10 ? '0' : '') + sec) : ' ');
    };
    this.setIconTrack = function(icon) {
        iconStation.html(icon);
    };
    var status = 'stopped';
    this.isPlayed = function() {
        return status == 'played';
    };
    this.isStopped = function() {
        return status == 'stopped';
    };
    this.checkPlay = function() {
        itemDOM.addClass('played');
        if (status == 'paused') {
            itemDOM.removeClass('paused');
            playIcon.removeClass('fa-pause');
        }
        status = 'played';
        playIcon.addClass('fa-play');
    };
    this.Play = function() {
        cbs.onplay(item);
        return item;
    };
    this.Stop = function() {
        if (status == 'paused') {
            itemDOM.removeClass('paused');
            playIcon.removeClass('fa-pause');
        } else {
            playIcon.removeClass('fa-play');
            itemDOM.removeClass('played');
        }
        cbs.onstop(item);
        status = 'stopped';
        return item;
    };
    this.Check = function(forPlay) {
        forPlay = forPlay || false;
        itemDOM.addClass(forPlay ? 'checked' : 'for-edit-check');
        return this;
    };
    this.unCheck = function(forPlay) {
        forPlay = forPlay || false;
        itemDOM.removeClass(forPlay ? 'checked' : 'for-edit-check');
        return this;
    };
    this.Pause = function() {
        itemDOM.removeClass('played').addClass('paused');
        playIcon.removeClass('fa-play').addClass('fa-pause');
        status = 'paused';
        return item;
    };
    this.getJQ = function() {
        return itemDOM;
    };
    var item = this;
    itemDOM.bind('dblclick', function() {
        playItem(item);
    });
    itemDOM.bind('click', function(e) {
        addToCheckList(e.ctrlKey, item);
    });
};
var addToCheckList = function(ctrl, item) {
    if (!ctrl) {
        for (var i = 0; i < checkList.length; i++) {
            checkList[i].unCheck();
        }
        checkList = [item.Check()];
    } else {
        var newCheckList = [];
        var checkedItem = false;
        for (var i = 0; i < checkList.length; i++) {
            if (checkList[i] == item) {
                checkList[i].unCheck();
                checkedItem = true;
            } else {
                newCheckList.push(checkList[i]);
            }
        }
        if (!checkedItem) {
            newCheckList.push(item.Check());
        }
        checkList = newCheckList;
    }
    if (!checkList.length) {
        deleteButton.addClass('disabled')
    } else {
        deleteButton.removeClass('disabled')
    }
};
var createPlayListItem = function(opts, callbacks) {
    var item = new playListItem(opts, callbacks);
    if (!playlist.length) {
        emptyPlayListTR.detach();
        playButton.removeClass('disabled');
    }
    playlist.push(item);
    if (!checkedItem) {
        checkedItem = item.Check(true);
    }
    checkPrevNextButtons();
    callbacks.onaddtoplaylist(item, opts);
    tracksList.append(item.getJQ());
};
var createOneItemChannels = function(opts, cbs) {
    var item = $('<li>');
    item.append('<img src="' + opts['image'] + '" alt=""/>', opts['name']).
        bind('dblclick', function() {
            createPlayListItem(opts, cbs);
        });
    return item;
};
this.addServiceList = function(options) {
    var stationBlock = $('<div>').addClass('one-station-block').appendTo(listServices);
    var channelsList = $('<ul>').addClass('channels-list');
    for (var k = 0; k < options['items'].length; k++) {
        channelsList.append(createOneItemChannels(options['items'][k], options['callbacks']));
    }
    var nameStationBlock = $('<div>').addClass('title-service');
    nameStationBlock.append('<img src="' + options['logotype'] + '" alt=""/>', options['name']);
    stationBlock.append(nameStationBlock);
    activateChannelsList(nameStationBlock, channelsList, stationBlock);
};
this.loadExtensions(extensions);
var applicationWindow = false;
var onOpenWindow = function() {
    applicationWindow.setTitle('Музыкальный проигрыватель');
};
var onCloseApplication = function() {

};
var app = this;
var _activate = function() {
    app.call('Windows', 'Window', {
        content: player,
        callbacks: {
            onopen: function(Window) {
                applicationWindow = Window;
                onOpenWindow();
            },
            onclose: function() {
                return false;
            },
            onactivate: function() {}
        },
        maximize: false,
        minimize: true
    });
};
var publicMethods = {
    _activate: _activate
};


this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};
this._constructor = function() {
    this.Event.addHandlers({
        onClose: onCloseApplication
    });
    this.call('Audio', 'getAudioSource', {
        source: 'audio',
        equalizer: true,
        analyser: {
            timeOut: false,
            fftSize: impuls * 2,
            update: AnalyserObject.update
        },
        callback: iniAudioJS
    });
    this.onInit();
};
