var nameStation = 'Radiotunes';
var historyURL = "http://api.audioaddict.com/v1/di/track_history/channel/{{id}}.jsonp";
var channelsURL = "http://listen.radiotunes.com/webplayer/{{key}}.jsonp";

var requestList = [];
var loadedHistory = {};
var sources = {};
var updateNameTrackInterval = false;
var path, thApp;
var playedItem = false;

var loadStationsList = function(cb) {
    $.ajax({
        url: path + '/playlist.json',
        dataType: 'JSON',
        success: cb
    });
};
var parseListSource = function(data) {
    var parsedSourceURL = data[0].split('.');
    parsedSourceURL.pop();
    var url = parsedSourceURL.join('.');
    return url.replace(/\.flv/, '');
};
var getChannelSource = function(key, cb) {
    if (sources[key]) { return cb(sources[key]) }
    $.ajax({
        dataType: "jsonp",
        url: channelsURL.replace(/\{\{key\}\}/, key),
        jsonpCallback: 'RadiotunesChannelSource',
        jsonp: 'callback',
        crossDomain: true,
        success: cb,
        error: function() {
            getChannelSource(key, cb);
        }
    });
};
var newSender = false;
var sendHistoryRequest = function() {
    if (!requestList.length || newSender) return;
    newSender = true;
    var params = requestList[0];
    $.ajax({
        dataType: "jsonp",
        url: historyURL.replace(/\{\{id\}\}/, params['id']),
        jsonpCallback: 'RadiotunesHistoryChannel',
        jsonp: 'callback',
        crossDomain: true,
        success: function(data) {
            var originalTime = thApp.getServerTime();
            var lastData = data[0];
            //--- Удаляем дубликаты запроса :) ---
            var newRequestList = [];
            for (var i = 0; i < requestList.length; i++) {
                requestList[i]['id'] != params['id'] ? newRequestList.push(requestList[i]) : false;
            }
            requestList = newRequestList;
            //--- Удаляем дубликаты запроса :) ---
            params['cb'](lastData, (lastData.started + lastData.duration) * 1000 - originalTime);
            loadedHistory[params['id']] = lastData;
            newSender = setTimeout(function() {
                newSender = false;
                sendHistoryRequest();
            }, 2500);
        },
        timeout: 2500,
        complete: function(xhr, status) {
            status == 'timeout' ? sendHistoryRequest() : false;
        }
    });
};

var getHistory = function(id, cb) {
    if (loadedHistory[id] && loadedHistory[id].length) {
        var hist = loadedHistory[id] || false, originalTime = thApp.getServerTime();
        if (hist) {
            var endTrack = (hist.started + hist.duration) * 1000;
            if (originalTime < endTrack) {
                cb(hist, endTrack - originalTime);
                return;
            } else {
                loadedHistory[id] = false;
            }
        }
    }
    requestList[playedItem && (id == playedItem.getOption('id')) ? 'unshift' : 'push']({id: id, cb: cb});
    if (requestList.length == 1) {
        sendHistoryRequest();
    }
};

var onStopPlay = function(item) {
    if (playedItem == item) {
        playedItem = false;
        updateNameTrackInterval ? clearTimeout(updateNameTrackInterval) : false;
        updateNameTrackInterval = false;
    }
};

var updatePlayingInfo = function(playListItem, newItemPlaying) {
    getHistory(playListItem.getOption('id'), function(data, endTrack) {
        if (playListItem !== playedItem) return;
        updateItem(playListItem, data);
        var channelName = playListItem.getOption('name');
        if (!data['ad']) {
            var trackFull = data['track'] ? data['track'] : (data['title'] + (data['release'] ? ' (' + data['release'] + ')' : ''));
            thApp.call('Notices', 'Notice', {
                text: trackFull,
                title: nameStation + '-' + channelName,
                image: data['art_url'] ? data['art_url'] + '?size=50' : playListItem.getOption('cover')
            });
            thApp.setWindowName(channelName + ' - ' + trackFull);
        } else {
            if (newItemPlaying) {
                thApp.call('Notices', 'Notice', {
                    text: channelName,
                    title: nameStation,
                    image: playListItem.getOption('cover')
                });
            }
            thApp.setWindowName(nameStation + ' - ' + channelName);
            endTrack = 120000;
        }
        updateNameTrackInterval = setTimeout(function() {
            updatePlayingInfo(playListItem);
        }, endTrack + 500);
    });
};

var onStartPlay = function(playListItem) {
    var key = playListItem.getOption('key');
    var newItemPlaying = playedItem != playListItem;
    if (playedItem != playListItem) {
        playedItem = playListItem;
        updateNameTrackInterval ? clearTimeout(updateNameTrackInterval) : false;
        updateNameTrackInterval = false;
    }
    getChannelSource(key, function(data) {
        thApp['setAudioSource'](
            playListItem,
            parseListSource(sources[key] = data)
        );
        updatePlayingInfo(playListItem, newItemPlaying);
    });
};

var onAddToPlayList = function(playListItem, opts) {
    playListItem['setNameTrack'](opts['name']);
    playListItem['setAuthorTrack'](nameStation);
    playListItem['setLengthTrack'](0);
    playListItem['setIconTrack']('<img src="' + path + '/logo.png' + '" alt=""/>');
    getHistory(opts['id'], function(data) {
        updateItem(playListItem, data);
    });
};
var updateItem = function(playListItem, opts) {
    var track = opts['track'].split('-');
    playListItem.setNameTrack(opts['ad'] ? playListItem.getOption('name') : ($.trim(track[1]) || opts['title']) + (opts['release'] ? ' (' + opts['release'] + ')' : ''));
    playListItem.setAuthorTrack(opts['ad'] ? nameStation : playListItem.getOption('name') + ' - ' + ($.trim(track[0]) || opts['artist']));
};
var createPlayList = function(data) {
    var list = [];
    for (var i = 0; i < data.length; i++) {
        list.push({
            image: data[i]['cover'] + '?size=18x18',
            cover: data[i]['cover'] + '?size=64x64',
            name: data[i]['name'],
            id: data[i]['id'],
            key: data[i]['key']
        });
    }
    return {
        logotype: path + '/logo.png',
        name: nameStation,
        items: list,
        callbacks: {
            onplay: onStartPlay,
            onstop: onStopPlay,
            onaddtoplaylist: onAddToPlayList
        }
    };
};

this._initialize = function(app, options) {
    thApp = app;
    path = options['path'];
    loadStationsList(function(data) {
        app['addServiceList'](createPlayList(data));
    });
};