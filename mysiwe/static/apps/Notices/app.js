var noticesBlock = $('<div>').addClass('notice-app-container');
var notices = {};
var showTime = 5000;
var transitionTime = 400;
var Notice = function() {
    var removeNotice = function() {
        appNoticeBlock.removeClass('notice-active');
        hideTimeout = setTimeout(function() {
            appNoticeBlock.detach();
            hideTimeout = false;
        }, transitionTime);
    };
    var hideNotice = function() {
        hideTimeout = setTimeout(removeNotice, showTime);
    };
    var appNoticeBlock = $('<div>').addClass('notice-app-block').css({
        transition: transitionTime + 'ms linear'
    }).bind('mouseenter', function() {
        clearTimeout(hideTimeout);
    }).bind('mouseleave', hideNotice).
    bind('click', removeNotice);
    var contentNoticeBlock = $('<div>').addClass('notice-app-block-content').appendTo(appNoticeBlock);
    var textBlock = $('<div>').addClass('notice-text-content').appendTo(contentNoticeBlock);
    var titleBlock = $('<div>').addClass('notice-title');
    var textContentBlock = $('<div>').addClass('notice-text');
    var image =         new Image();
    var onloadImage =   new Image();
    var hideTimeout = false;
    var showNotice = function(params) {
        $(onloadImage).unbind();
        params.title ? titleBlock.text(params.title).prependTo(textBlock) : titleBlock.detach();
        params.text ? textContentBlock.text(params.text).appendTo(textBlock) : textContentBlock.detach();
        if (onloadImage.src) {
            image.src = onloadImage.src;
            contentNoticeBlock.prepend(image);
            onloadImage.src = '';
        }
        appNoticeBlock.prependTo(noticesBlock);
        setTimeout(function() {appNoticeBlock.addClass('notice-active')}, 10);
        hideNotice();
    };
    this.showNotice = function(params) {
        if (params.image) {
            $(onloadImage).bind('load', function() {
                hideTimeout ? clearTimeout(hideTimeout) : false;
                showNotice(params);
            });
            onloadImage.src = params.image;
        } else {
            $(image).detach();
            hideTimeout ? clearTimeout(hideTimeout) : false;
            showNotice(params);
        }
    };
};

var showNotice = function(options, app) {
    if (!notices[app.name]) {
        notices[app.name] = new Notice();
    }
    notices[app.name].showNotice(options);
};

var publicMethods = {
    Notice: showNotice
};

this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};

this._constructor = function() {
    this.call('AppsSection', 'append', {
        content: noticesBlock
    });
    this.onInit();
};
