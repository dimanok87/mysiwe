var autoStart = {
    "Clock": false
};
var systemApps = ['Sidebar', 'AppsSection', 'TopBar', 'Windows', 'Notices', 'Audio'];
var paramsApps = {
    'userApplication': {
        url: 'http://cupy.ru/mysiwe/?app=messenger'
    }
};
var menuItems = [
    {item: 'Profile', action: false},
    {item: 'System settings', action: false},
    {item: 'Exit', link: '/logout/'}
];

var topBarMenu = $('<ul>').addClass('os-app-top-menu top-bar-menu');

var menuItem = function(params) {
    var item = $('<li>').text(params['item']);
    params['action'] ?
        item.on('click', params['action']) :
        false;
    params['link'] ? item.on('click', function() {
        window.location.href = params['link'];
    }) : false;
    return item;
};
for (var i = 0; i < menuItems.length; i++) {
    topBarMenu.append(menuItem(menuItems[i]));
}
var topBarButton = $('<div>').addClass('os-app-top-button').append(
    $('<div>').addClass('fa fa-cog')
);

this._constructor = function() {
    this.call('Background', 'setBackground', {
        src: '/static/images/123.jpg'
    });
    this.call('TopBar', 'append', {
        content: topBarButton
    });
    this.call('TopBar', 'addContentToButton', {
        content: topBarMenu
    });
    for (var s = 0; s < systemApps.length; s++) {
        this.call(systemApps[s], false, paramsApps[systemApps[s]] || false);
    }
    for (var k in autoStart) {
        this.call(k, false, autoStart[k]);
    }
    this.onInit();
};
