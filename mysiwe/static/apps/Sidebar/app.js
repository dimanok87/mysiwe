var content = $("<div>").addClass('sidebar-app');
var iconSize = 40;

var appsSidebar = ["Mysiwe", "Music"]; // "CloudStorage"

var mask = $("<img>");
var button = $("<div>").addClass('icon-sidebar');
var maskHolder = $("<div>").addClass('mask-holder');
var application = this;
var canvasForIcon = document.createElement('canvas');
canvasForIcon.width = iconSize;
canvasForIcon.height = iconSize;

var context = canvasForIcon.getContext("2d");

var getIconBase64 = function(src, cb) {
    var img = new Image();
    img.onload = function() {
        context.clearRect(0, 0, iconSize, iconSize);
        context.drawImage(img, 0, 0, iconSize, iconSize);
        cb(canvasForIcon.toDataURL());
    };
    img.src = src;
};

var applications = {};


var publicMethods = {
    append: function(options, app) {
    },
    addContentToButton: function(options, app) {
    },
    getPosition: function(options) {
        options['callback'](applications[options['appName']]['getPosition']());
    }
};
this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};
var sideBarIcon = function(appName) {
    var createdButton = button.clone().
            width(iconSize).
            height(iconSize).append(
            maskHolder.clone()
        ).attr('data-title', appName);

    //--- Переместить этот кусок в OS и оттуда уже строить sidebar при старте ---
    application.getAppInfo(appName, function(data) {
        getIconBase64(data['path'] + data['data']["iconsPath"] + '/' + data['data']['icons']['64'], function(src) {
            createdButton.css({
                background: 'url(' + src + ') center center no-repeat'
            });
        });
    });
    //--- Переместить этот кусок в OS и оттуда уже строить sidebar при старте ---

    createdButton.click(function() {
        application.call(appName, '_activate', false);
    });
    this.getJQ = function() {
        return createdButton;
    };
    this.getPosition = function() {
        var offset = createdButton.offset();
        return {
            width: iconSize,
            height: iconSize,
            left: offset.left,
            top: offset.top
        };
    };
};

this._constructor = function() {
    this.call('Desktop', 'append', {content: content.addClass('active')});
    for (var i = 0; i < appsSidebar.length; i++) {
        var icon = new sideBarIcon(appsSidebar[i]);
        applications[appsSidebar[i]] = icon;
        content.append(icon.getJQ());
    }
    this.onInit();
};