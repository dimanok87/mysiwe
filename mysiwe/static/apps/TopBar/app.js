var topbar = $("<div>").addClass('topbar-app');
var contentsBar = {};

var appsInBar = {
    "Audio": true,
    "Clock": true,
    "OS": true
};


var createContentButton = function(appName) {
    if (contentsBar[appName]) return;
    var button = $('<div>').addClass('top-bar-button').attr('tabindex', 1);

    contentsBar[appName] = {
        button: button
    };

    if (appsInBar[appName]) {
        for (var i in appsInBar) {
            if (contentsBar[i]) {
                topbar.prepend(contentsBar[i]['button']);
            }
        }
    } else {
        topbar.append(contentsBar[appName]['button']);
    }

    button.on('focus', function() {
        if (!contentsBar[appName]['content']) return;

        setTimeout(function() {
            contentsBar[appName]['content'].css({marginLeft: ''});
            var offset = contentsBar[appName]['content'].offset();
            contentsBar[appName]['content'].css({
                marginLeft: Math.min(0, win.width() - (offset.left + contentsBar[appName]['content'].outerWidth()))
            });
        }, 1);
    }).on('mousedown', function(e) {
        var target = $(e.target);
        if (button.is(':focus') && !target.parents(contentsBar[appName]['content']).length) {
            button.blur();
            return false;
        }
    });
};


var publicMethods = {
    append: function(options, app) {
        var button = options["content"];
        createContentButton(app.name);
        contentsBar[app.name]['button'].append(button);
    },
    addContentToButton: function(options, app) {
        var content = options["content"];
        !createContentButton(app.name);
        contentsBar[app.name]['content'] = contentsBar[app.name]['content'] || $('<div>').addClass('top-bar-content-button');
        contentsBar[app.name]['content'].appendTo(contentsBar[app.name]['button']);
        contentsBar[app.name]['content'].append(options["content"]);
    },
    getSize: function(callback) {
        callback(topbar.height());
    }
};


this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};


this._constructor = function() {
    this.call('Desktop', 'append', {content: topbar});
    this.onInit();
};



