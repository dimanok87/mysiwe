var application = this;

var WIND_CONSTS_CLASS = {
    "window":   "mysiwe-window",
    "head":     "mysiwe-window-head",
    "content":  "mysiwe-window-content",
    "name":     "mysiwe-window-name",
    "moved":    "moved-window",
    "active":   "active-window",
    "buttons":  "mysiwe-window-buttons",
    "button":   "mysiwe-window-button",
    "close-button": "mysiwe-window-close-button",
    "minimize-button": "mysiwe-window-minimize-button",
    "maximize-button": "mysiwe-window-maximize-button"
};

var mask = $("<div>").css({
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 999,
    cursor: "move"
});

var OS = {
    Windows: navigator.platform.indexOf("Win") > -1,
    Mac: navigator.platform.indexOf("Mac") > -1,
    Linux: navigator.platform.indexOf("Linux") > -1
};

var windowParams = {
    'buttons': {
        position: OS.Windows ? 'right' : 'left'
    }
};


var activeWindow = false;

var Window = function(options, app) {
    options = $.extend({
        'buttons': {},
        'callbacks': {},
        'width': 800,
        'height': 600,
        'resizable': {
            'width': {
                min: 600,
                max: '100%'
            },
            'height': {
                min: 400,
                max: '100%'
            }
        }
    }, options);
    var nameApp =   $("<span>").addClass(WIND_CONSTS_CLASS["name"]);
    var wind =      $("<div>").addClass(WIND_CONSTS_CLASS["window"]).css({
        width: options['width'],
        height: options['height']
    });
    var head =      $("<div>").addClass(WIND_CONSTS_CLASS["head"]);
    var cont =      $("<div>").addClass(WIND_CONSTS_CLASS["content"]);
    var buttons =   $("<div>").addClass(WIND_CONSTS_CLASS["buttons"] + ' position-' + windowParams['buttons']['position']).
        appendTo(head);

    var windowSize = {
        width: options['width'],
        height: options['height']
    };

    this.setTitle = function(titleWindow) {
        nameApp.text(titleWindow);
    };

    var closeWindow = function(abs) {
        abs = abs || false;
        wind.css({
            transition: '100ms linear',
            transform: 'scale(1.2, 1.2)',
            opacity: 0
        });
        setTimeout(function() {
            wind.detach();
            wind.css({
                transition: '',
                transform: '',
                opacity: ''
            });
            !abs && options.callbacks.onclose ? options.callbacks.onclose() : app.Close();
        }, 200);
    };

    var maximized = false;
    var unMaximizeWindow = function() {
        wind.css({
            left: position['left'],
            top: position['top'],
            width: windowSize['width'],
            height: windowSize['height']
        }, 400).removeClass('full-size');
        maximized = false;
        options.callbacks.onunmaximize ? options.callbacks.onunmaximize() : false;
    };
    var maximizeWindow = function() {
        if (maximized) return unMaximizeWindow();
        wind.css({
            left: 0,
            top: 0,
            width: '100%',
            height: '100%'
        }, 400).addClass('full-size');
        maximized = true;
        options.callbacks.onmaximize ? options.callbacks.onmaximize() : false;
    };

    var minimizeWindow = function() {
        application.call('AppsSection', 'getSize', function(sizes) {
            topbarHeight = sizes.height;
            sideBarWidth = sizes.width;
            application.call('Sidebar', 'getPosition', {
                callback: function(data) {
                    var fW = wind.width(),
                        fH = wind.height();
                    wind.css({
                        transition: '200ms linear',
                        transform: 'scale(' + data.width / fW + ', ' + data.height / fH + ')',
                        left: data['left'] - (fW - data.width) / 2 - (win.width() - sideBarWidth),
                        top: data['top'] - (fH - data.height) / 2 - (win.height() - topbarHeight),
                        opacity: 0
                    });
                    setTimeout(function() {
                        wind.detach();
                        options.callbacks.onminimize ? options.callbacks.onminimize() : false;
                        wind.css({
                            transition: '',
                            transform: '',
                            left: position['left'],
                            top: position['top'],
                            opacity: 1
                        });
                    }, 300);
                },
                appName: app.name
            });
        });
    };

    this.deleteWindow = function() {};
    var deactivateWindow = this.deactivateWindow = function() {
        if (activeWindow != thWindow) return;
        wind.removeClass( WIND_CONSTS_CLASS['active']);
        activeWindow = false;
    };
    var thWindow = this;
    var winDOM = wind.get(0);
    var activateWindow = this.activateWindow = function(event) {
        if (activeWindow == thWindow) return;
        wind.addClass(WIND_CONSTS_CLASS['active']);
        activeWindow ? activeWindow.deactivateWindow() : false;
        activeWindow = thWindow;
        if (event) {
            win.bind('mouseup', mouseUpAfterActive);
        } else {
            if (maximized) {
                wind.css({left: 0, top: 0});
            }
            application.call('AppsSection', 'append', {content: wind});
        }
    };
    var mouseUpAfterActive = function(e) {
        var target = $(e.target);
        win.unbind('mouseup', mouseUpAfterActive);
        var downed = downedButton;
        downedButton = false;
        if (target.is(downed)) return;
        setTimeout(function() {
            application.call('AppsSection', 'append', {content: wind})
        }, 10);
    };
    win.mousedown(function(e) {
        var t = $(e.target);
        if (!(t.is(wind) || wind.find(t).length)) {
            deactivateWindow();
        }
    });
    wind.bind('mousedown', activateWindow);

    var downedButton = false;
    var activateButtonDown = function() {
        downedButton = this;
    };
    //--- Кнопка закрытия окна ---
    $("<div>").addClass(WIND_CONSTS_CLASS["button"] + ' ' + WIND_CONSTS_CLASS["close-button"]).
        appendTo(buttons).bind({
            'click': function(){
                closeWindow();
            },
            'mousedown': activateButtonDown
        });

    //--- Кнопка минимизации окна ---
    if (options['minimize']) {
        $("<div>").addClass(WIND_CONSTS_CLASS["button"] + ' ' + WIND_CONSTS_CLASS["minimize-button"]).
            appendTo(buttons).bind({
                'click': minimizeWindow,
                'mousedown': activateButtonDown
            });
    }

    //--- Кнопка раскрытия окна на весь экран ---
    if (options['maximize']) {
        head.bind('dblclick', function() {
            maximizeWindow();
        });
        $("<div>").addClass(WIND_CONSTS_CLASS["button"] + ' ' + WIND_CONSTS_CLASS["maximize-button"]).
            appendTo(buttons).bind({
                'click': maximizeWindow,
                'mousedown': activateButtonDown
            });
    }

    //--- Полоса для растягивания по горизонтали ---
    if (options['resizable']['width']) {
        //$("<div>").addClass(WIND_CONSTS_CLASS["button"] + ' ' + WIND_CONSTS_CLASS["maximize-button"]).
        //    appendTo(buttons).bind({
        //        'click': '',
        //        'mousedown': activateButtonDown
        //    });
    }

    //--- Полоса растягивания по вертикали ---
    if (options['resizable']['height']) {
        //$("<div>").addClass(WIND_CONSTS_CLASS["button"] + ' ' + WIND_CONSTS_CLASS["maximize-button"]).
        //    appendTo(buttons).bind({
        //        'click': '',
        //        'mousedown': activateButtonDown
        //    });
    }

    //--- Угловое растягивание ---
    if (options['resizable']['height'] && options['resizable']['width']) {
        //$("<div>").addClass(WIND_CONSTS_CLASS["button"] + ' ' + WIND_CONSTS_CLASS["maximize-button"]).
        //    appendTo(buttons).bind({
        //        'click': '',
        //        'mousedown': activateButtonDown
        //    });
    }




    var position = {left: 0, top: 0};
    var movedPosition = {left: 0, top: 0};
    var startMovedPosition = {left: 0, top: 0};
    wind.append(
        head.append(nameApp),
        cont.append(options['content'])
    );
    var topbarHeight = 0;
    var sideBarWidth = 0;
    var startMoviengLeft = 0;
    var downHeader = function(e) {
        var target = $(e.target);
        if (target.is(buttons) || buttons.find(target).length) return;
        e.preventDefault();
        startMoviengLeft = e.offsetX;
        startMovedPosition = {left: e.clientX, top: e.clientY};
        movedPosition = {left: position.left - e.clientX, top: position.top - e.clientY};
        head.unbind("mousedown", downHeader);
        win.bind({"mousemove": startMoveWindow, "mouseup": dropWindowNoMove});
        application.call('AppsSection', 'getSize', function(sizes) {
            topbarHeight = sizes.height;
            sideBarWidth = sizes.width;
        });
    };
    var dropWindowNoMove = function() {
        win.unbind({"mousemove": startMoveWindow, "mouseup": dropWindowNoMove});
        head.bind("mousedown", downHeader);
        mask.detach();
    };
    var startMoveWindow = function(e) {
        if (!maximized) {
            wind.addClass(WIND_CONSTS_CLASS["moved"]);
            win.unbind({"mousemove": startMoveWindow, "mouseup": dropWindowNoMove});
            win.bind({"mousemove": moveWindow, "mouseup": dropWindow});
        } else {
            if (
                Math.abs(startMovedPosition['left'] - e.clientX) > 10 ||
                Math.abs(startMovedPosition['top'] - e.clientY) > 10
            ) {
                var offsetMinWindow = (1 + (startMoviengLeft / sideBarWidth - 1)) * windowSize['width'];
                position = {
                    left:  startMoviengLeft - offsetMinWindow,
                    top: e.clientY - startMovedPosition['top']
                };
                movedPosition = {left: position.left - e.clientX, top: position.top - e.clientY};
                unMaximizeWindow();
                win.unbind({"mousemove": startMoveWindow, "mouseup": dropWindowNoMove});
                win.bind({"mousemove": moveWindow, "mouseup": dropWindow});
            }
        }
        mask.appendTo(body);
    };

    var moveWindow = function(e) {
        winDOM.style.left = e.clientX + movedPosition.left + 'px';
        winDOM.style.top = e.clientY + movedPosition.top + 'px';
    };
    var dropWindow = function(e) {
        wind.removeClass(WIND_CONSTS_CLASS["moved"]);
        head.bind("mousedown", downHeader);
        position = {
            left: Math.max(0, Math.min(sideBarWidth - wind.outerWidth(), e.clientX + movedPosition.left)),
            top: Math.max(0, Math.min(topbarHeight - wind.outerHeight(), e.clientY + movedPosition.top))
        };
        winDOM.style.left = position.left + 'px';
        winDOM.style.top = position.top + 'px';
        win.unbind({"mousemove": moveWindow, "mouseup": dropWindow});
        mask.detach();
    };
    head.bind('mousedown', downHeader);
    activateWindow();
};
var appsWindows = {};
var publicMethods = {
    Window: function(options, app) {
        options['callbacks'] = options['callbacks'] || {};
        appsWindows[app.name] = appsWindows[app.name] || [];
        if (appsWindows[app.name].length && !options['parent']) {
            options['callbacks']['onactivate'] ?
                options['callbacks']['onactivate']([appsWindows[app.name][0]]) :
                false;
            appsWindows[app.name][0].activateWindow();
            return;
        }
        var newWindow = new Window(options, app);
        appsWindows[app.name].push(newWindow);
        options['callbacks']['onopen'] ?
            options['callbacks']['onopen'](newWindow) :
            newWindow;
    }
};
this.publicMethod = function(appMethod, callOptions, subjectApp) {
    if (publicMethods[appMethod]) {
        publicMethods[appMethod](callOptions, subjectApp);
    }
};

this._constructor = function() {
    this.onInit();
};
