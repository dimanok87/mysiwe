var STATUSES = {"IN_LOAD": 1, "ON_LOAD": 2, "STARTED": 3};


//--- Набор объектов приложений ---
window.applications = {};
var appsConfigs = {};

//-- Объект обработчика событий --//
var Events = function() {
    var handlers = {}, application = false;
    //--- Метод добавляет обработчик "b" для события "a" ---
    this.addHandlers = function(a, b) {
        if (typeof a == "object") {
            for (var i in a) {
                this.addHandlers(i || false, a[i || false]);
            }
        } else if (b || false) {
            (handlers[a] = handlers[a] || []).push(b);
            if (a == "onInit" && application && application["status"] == STATUSES["STARTED"]) {
                this.callHandlers(a);
                this.removeHandlers(a);
            }
        }
    };
    //--- Метод вызывает обработчик ---
    this.callHandlers = function(a, params) {
        for (var i = 0; i < (handlers[a] || []).length; i++) {
            handlers[a][i](params || application["application"]);
        }
    };
    //--- Метод удаляет обработчики ---
    this.removeHandlers = function(a) {
        if (a) {
            handlers[a] = false;
        } else {
            handlers = {};
        }
    };
    //--- Метод установки приложения ---
    this.setApplication = function(app) {
        application = applications[app];
    };
};

/* Загрузка приложений */
var systemTime = false;
var loadApplication = function(appName, callback) {
    var path = systemParams["ApplicationPath"].replace(/\{\{APP_NAME\}\}/g, appName);
    jQuery.ajax({
        url: path + 'app.js',
        cache: true,
        dataType: 'text',
        success: function(data, status, xhr) {
            if (data) {
                callback(Function(data), path);
            }
            if (systemTime) return;
            systemTime = {
                serverTime: new Date(xhr.getResponseHeader('Date')).getTime(),
                localTime: new Date().getTime()
            };
            systemTime['rangeTime'] = systemTime['serverTime'] - systemTime['localTime'];
        }
    });
};


var loadUserApplication = function(appName, path, callback) {
    jQuery.ajax({
        dataType: "jsonp",
        url: path,
        jsonpCallback: 'startUserApp',
        jsonp: 'callback',
        crossDomain: true,
        success: function(data) {
            if (data && data['app']) {
                callback(Function(data['app']), path);
            }
        }
    });
};

/* Загрузка дополнений приложения */
var loadExtension = function(appName, extensionName, callback) {
    var url =
        systemParams["ApplicationPath"].
            replace(/\{\{APP_NAME\}\}/g, appName) +
        systemParams["PluginsPath"]["path"].
            replace(/\{\{PLUGIN_NAME\}\}/g, extensionName);
    jQuery.ajax({
        url: url + "/" + systemParams["PluginsPath"]["ext"],
        cash: false,
        dataType: 'text',
        success: function(data) {
            if (data) {
                callback(Function(data), url);
            }
        }
    });
};

//--- Функция запуска приложений ---
var appConfig = function(appName) {
    var loadStatus = false;
    var appPath = systemParams["ApplicationPath"].replace(/\{\{APP_NAME\}\}/g, appName);
    var Event = new Events();
    var configJSON = false;
    this.getConfig = function() {
        return configJSON;
    };
    this.loadConfig = function(callback) {
        if (loadStatus !== 'loaded') {
            Event.addHandlers({
                'onLoadParams': callback
            })
        } else {
            callback({
                path: appPath,
                data: configJSON
            });
            return;
        }
        $.ajax({
            url: appPath + 'config.json',
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                configJSON = data;
                Event.callHandlers('onLoadParams', {
                    path: appPath,
                    data: data
                });
                loadStatus = 'loaded';
            }
        });
    };
};

var startApplication = function(name, app, options, path) {
    app.prototype.Close = function() {
        this.Event.callHandlers("onClose");
        this.Event.removeHandlers();
        delete applications[name]["application"];
        applications[name]["status"] = STATUSES["ON_LOAD"];
    };
    app.prototype.onInit = function() {
        applications[name]["status"] = STATUSES["STARTED"];
        this.Event.callHandlers("onInit");
        this.Event.removeHandlers("onInit");
    };
    app.prototype.getServerTime = function() {
        return systemTime['rangeTime'] + (new Date()).getTime();
    };
    app.prototype.config = appsConfigs[name];
    app.prototype.Event = applications[name]["Event"];
    app.prototype.path = path;
    app.prototype.loadExtensions = function(extensions) {
        var _app = this;
        for (var k = 0; k < extensions.length; k++) {
            loadExtension(name, extensions[k], function(script, extUrl) {
                var plugin = new script();
                plugin["_initialize"](_app, {
                    "path": extUrl
                });
            });
        }
    };
    app.prototype.getAppInfo = function(appName, callback) {
        if (!appsConfigs[appName]) {
            appsConfigs[appName] = new appConfig(appName);
        }
        appsConfigs[appName].loadConfig(callback);

    };
    app.prototype.call = function(appName, appMethod, callOptions) {
        callOptions = callOptions || {};
        if (!applications[appName] || applications[appName]["status"] == STATUSES["ON_LOAD"]) {
            callOptions['initiate'] = this;
            openApplication(appName, callOptions);
        }
        applications[appName].Event.addHandlers({
            onInit: function(calledApp) {
                calledApp['publicMethod'] ? calledApp['publicMethod'](appMethod, callOptions, _app) : false;
            }
        });
    };
    app.prototype.name = name;
    var _app = new app();
    applications[name] = {
        "application": _app,
        "prototype": app,
        "status": STATUSES["ON_LOAD"],
        "Event": applications[name]["Event"]
    };
    _app["Event"].setApplication(name);
    _app["_constructor"](options);
};

var Application = function(name, options) {
    options = options || {};
    applications[name] = {
        Event: new Events(),
        status: STATUSES["IN_LOAD"]
    };
    applications[name]['Event'].setApplication(name);
    if (!appsConfigs[name]) {
        appsConfigs[name] = new appConfig(name);
    }
    appsConfigs[name].loadConfig(function() {
        if (!options['url']) {
            loadApplication(name, function (app, path) {
                startApplication(name, app, options, path);
            });
        } else {
            loadUserApplication(name, options['url'], function (app, path) {
                startApplication(name, app, options, path);
            });
        }
    });
    return applications[name];
};

var openApplication = this.Application = function(name, options) {
    if (!applications[name]) {
        return Application(name, options);
    } else if (applications[name]["status"] == STATUSES["STARTED"]) {
        return applications[name]['application'];
    } else if (applications[name]["status"] == STATUSES["ON_LOAD"]) {
        applications[name]['application'] = new applications[name]['prototype']();
        applications[name]['application']["Event"].setApplication(name);
        applications[name]['application']["_constructor"](options);
        return applications[name]['application'];
    } else {
        return applications[name];
    }
};
