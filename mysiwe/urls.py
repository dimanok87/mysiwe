from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^', include('mysiwe.apps.users.urls', namespace='users')),
    url(r'^app/', include('mysiwe.apps.apps.urls', namespace='apps')),
    url(r'^admin/', include(admin.site.urls)),
)
