from django.conf import settings
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic import View


def get_form_errors(form):
    return {field: error[0] for field, error in form.errors.items()}


class CommonView(View):
    def get(self, request, *args, **kwargs):
        if not request.is_ajax():
            return super(CommonView, self).get(request, *args, **kwargs)
        else:
            return JsonResponse(self.get_ajax(request, *args, **kwargs))


class LoginRequired(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(LoginRequired, self).dispatch(request, *args, **kwargs)
        else:
            if not request.is_ajax():
                return HttpResponseRedirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
            else:
                return JsonResponse({
                    'status': 'error',
                    'code': 101,
                    'message': 'Auth required'
                })
